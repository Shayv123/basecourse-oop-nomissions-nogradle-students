package AIF.AerialVehicles;

import AIF.Entities.Coordinates;

public abstract class Module {
    private double range;

    public Module(double range) {
        this.range = range;
    }

    public double getRange() {
        return range;
    }

    public void activateModule(){}
}
