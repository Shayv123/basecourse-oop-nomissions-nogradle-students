package AIF.AerialVehicles;

import AIF.Entities.Coordinates;

public abstract class Hermes extends UAV {
    public Hermes(Coordinates currentLoc,Class[] compatiableModules,int moduleSpots) {
        super(currentLoc,10000,compatiableModules,moduleSpots);
    }
}
