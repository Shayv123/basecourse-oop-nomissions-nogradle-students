package AIF.AerialVehicles;

import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.Entities.Coordinates;

public abstract class UAV extends AerialVehicle {
    public UAV(Coordinates currentLoc,double maintenanceRange,Class[] compatiableModules,int moduleSpots) {
        super(currentLoc,maintenanceRange,compatiableModules,moduleSpots);
    }

    public void hoverOverLocation(double hours) throws CannotPerformOnGroundException {
        if(super.isAirborne()){
            super.setDistanceToRepair(super.getDistanceToRepair()-(150*hours));
            System.out.println("hovering over "+super.getCurrentLoc());
        }
        else {
            throw new CannotPerformOnGroundException();
        }
    }
}
