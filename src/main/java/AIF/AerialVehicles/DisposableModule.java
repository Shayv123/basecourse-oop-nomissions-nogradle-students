package AIF.AerialVehicles;

public abstract class DisposableModule extends Module {
    boolean isAvailable;

    public DisposableModule(double range) {
        super(range);
    }
}
