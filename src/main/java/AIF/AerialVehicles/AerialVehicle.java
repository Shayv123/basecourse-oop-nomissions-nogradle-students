package AIF.AerialVehicles;


import AIF.AerialVehicles.Exceptions.*;
import AIF.Entities.Coordinates;

import java.util.ArrayList;

public abstract class AerialVehicle {
    private boolean isAirborne;
    private Coordinates currentLoc;
    private double maintenanceRange;
    private Class[] compatibaleModules;
    private int moduleSpots;
    private ArrayList<Module> aplliedModules;
    private double distanceToRepair;

    public AerialVehicle(Coordinates currentLoc, double maintenanceRange, Class[] compatibaleModules, int moduleSpots) {
        this.currentLoc = currentLoc;
        this.isAirborne = false;
        this.maintenanceRange = maintenanceRange;
        this.distanceToRepair = maintenanceRange;
        this.compatibaleModules = compatibaleModules;
        this.moduleSpots = moduleSpots;
        this.aplliedModules = new ArrayList<>();
    }

    public Coordinates getCurrentLoc() {
        return this.currentLoc;
    }

    public double getMaintenanceRange() {
        return maintenanceRange;
    }

    public double getDistanceToRepair() {
        return this.distanceToRepair;
    }

    public void setDistanceToRepair(double distanceToRepair) {
        this.distanceToRepair = distanceToRepair;
    }

    public boolean isAirborne() {
        return this.isAirborne;
    }

    protected void changeState() {
        this.isAirborne = !this.isAirborne();
    }

    public void takeOff() throws CannotPerformInMidAirException {
        if (this.isAirborne) {
            throw new CannotPerformInMidAirException();
        } else {
            this.changeState();
            System.out.println("Taking off");
        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformOnGroundException {
        if (this.isAirborne) {
            this.currentLoc = destination;
            this.setDistanceToRepair(this.getDistanceToRepair() - this.currentLoc.distance(destination));
            System.out.println("flying to " + destination);
        } else {
            throw new CannotPerformOnGroundException();
        }
    }

    public void land() throws CannotPerformOnGroundException {
        if (this.isAirborne) {
            System.out.println("landing");
            this.changeState();
        } else {
            throw new CannotPerformOnGroundException();
        }
    }

    public boolean needMaintenance() {
        return this.distanceToRepair < 0;
    }

    public void performMaintenance() throws CannotPerformInMidAirException {
        if (!this.isAirborne()) {
            this.setDistanceToRepair(this.getMaintenanceRange());
            System.out.println("performing maintaince");
        } else {
            throw new CannotPerformInMidAirException();
        }
    }

    public void loadModule(Module moduleToAdd) throws ModuleNotCompatibleException, NoModuleStationAvailableException {
        if (isInArray(this.compatibaleModules, moduleToAdd.getClass())) {
            if (this.aplliedModules.size() == this.moduleSpots) {
                throw new NoModuleStationAvailableException();
            } else {
                this.aplliedModules.add(moduleToAdd);
                System.out.println("module of type: " + moduleToAdd.getClass() + " added successfully");
            }
        } else
            throw new ModuleNotCompatibleException();
    }

    public void activateModule(Class module, Coordinates target) throws ModuleNotFoundException, NoModuleCanPerformException {
        Module appliedModule = null;
        for (Module module1 : this.aplliedModules) {
            if (module1.getClass().equals(module)) {
                appliedModule = module1;
            }
        }
        if (appliedModule != null) {
            if (target.distance(currentLoc) > appliedModule.getRange()) {
                throw new NoModuleCanPerformException();
            } else {
                appliedModule.activateModule();
                if (module.isAssignableFrom(DisposableModule.class)) {
                    aplliedModules.remove(appliedModule);
                }
            }
        }
        else {
            throw new ModuleNotFoundException();
        }

    }

    private boolean isInArray(Class[] arrayToSearch, Class wordToFind) {
        for (int i = 0; i < arrayToSearch.length; i++) {
            if (arrayToSearch[i].equals(wordToFind)) {
                return true;
            }
        }
        return false;
    }
}
